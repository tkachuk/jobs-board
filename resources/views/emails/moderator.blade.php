Hi {{ $moderatorEmail }}! We have one new Job from.
Title - `{{ $title }}`
Description - `{{ $description }}`
Email - `{{ $hrEmail }}`

Please, <a href="{{ $approveLink }}">approve</a> it or <a href="{{ $spamLink }}">mark as spam</a>

==================================
