import AllJobs from './components/job/List.vue';
import AddJob from './components/job/Add.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllJobs
    },
    {
        name: 'add',
        path: '/add',
        component: AddJob
    }
];
