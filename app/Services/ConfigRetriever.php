<?php


namespace App\Services;

use Illuminate\Contracts\Config\Repository as ConfigRepository;

class ConfigRetriever
{
    /**
     * @var ConfigRepository
     */
    private $config;

    /**
     * @param ConfigRepository $config
     */
    public function __construct(ConfigRepository $config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getApplicationUrl()
    {
        return (string) $this->config->get('app.url');
    }

    /**
     * @return string
     */
    public function getModeratorEmail()
    {
        return (string) $this->config->get('moderation.email');
    }
}
