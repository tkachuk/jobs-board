<?php

namespace App\Services;

use App\Models\Job;

class JobPostingEmailNotifier implements JobPostingNotifierInterface
{
    /**
     * @var ConfigRetriever
     */
    private $config;

    /**
     * @var LinkGenerator
     */
    private $linkGenerator;

    public function __construct(ConfigRetriever $config, LinkGenerator $linkGenerator)
    {
        $this->config = $config;
        $this->linkGenerator = $linkGenerator;
    }

    /**
     * {@inheritDoc}
     */
    public function notifyAuthor(Job $job)
    {
        $data = [
            'email' => $job->email,
            'jobId' => $job->id,
        ];

        $message = view('emails.author', $data)->render();

        file_put_contents('/app/storage/logs/author.log', $message, FILE_APPEND);
    }

    /**
     * {@inheritDoc}
     */
    public function notifyModerator(Job $job)
    {
        $data = [
            'moderatorEmail' => $this->config->getModeratorEmail(),
            'jobId'          => $job->id,
            'title'          => $job->title,
            'description'    => $job->description,
            'hrEmail'        => $job->email,
            'approveLink'    => $this->linkGenerator->getApproveLink($job),
            'spamLink'       => $this->linkGenerator->getSpamLink($job),
        ];

        $message = view('emails.moderator', $data)->render();

        file_put_contents('/app/storage/logs/moderator.log', $message, FILE_APPEND);
    }
}
