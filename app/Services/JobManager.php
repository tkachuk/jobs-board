<?php

namespace App\Services;

use App\Models\Job;
use App\Repositories\JobRepository;

class JobManager implements JobManagerInterface
{
    /**
     * @var JobRepository
     */
    private $jobRepository;

    /**
     * @param JobRepository $jobRepository
     */
    public function __construct(JobRepository $jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $data): Job
     {
         return $this->jobRepository->create([
            'title' => $data['title'],
            'description' => $data['description'],
            'email' => $data['email'],
            'status' => $this->getJobStatus($data['email'])
         ]);
     }
    /**
     * {@inheritDoc}
     */

     public function updateStatus(int $id, string $hash, string $status): ?Job
     {
        $job = $this->jobRepository->findByIdAndHash($id, $hash);

        if ($job) {
            $job->status = $status;
            $job->save();
        }

        return $job;
     }

    /**
     * @param string $email
     * @return string
     */
     private function getJobStatus(string $email): string
     {
        return $this->jobRepository->allApprovedByEmail($email)->count()
            ? Job::STATUS_APPROVED
            : Job::STATUS_NEW;
     }
}
