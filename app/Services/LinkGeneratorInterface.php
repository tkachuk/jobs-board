<?php

namespace App\Services;

use App\Models\Job;

interface LinkGeneratorInterface
{
    /**
     * @param Job $job
     *
     * @return string
     */
    public function getApproveLink(Job $job): string;

    /**
     * @param Job $job
     * @return string
     */
    public function getSpamLink(Job $job): string;
}
