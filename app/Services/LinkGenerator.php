<?php

namespace App\Services;

use App\Models\Job;

class LinkGenerator implements LinkGeneratorInterface
{
    /**
     * @var ConfigRetriever
     */
    private $config;

    public function __construct(ConfigRetriever $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritDoc}
     */
    public function getApproveLink(Job $job): string
    {
        return sprintf(
        '%s/api/jobs/approve/%d/%s',
            $this->config->getApplicationUrl(),
            $job->id,
            $job->hash
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getSpamLink(Job $job): string
    {
        return sprintf(
            '%s/api/jobs/spam/%d/%s',
            $this->config->getApplicationUrl(),
            $job->id,
            $job->hash
        );
    }
}
