<?php

namespace App\Services;

use App\Models\Job;

interface JobPostingNotifierInterface
{
    /**
     * @param Job $job
     * @return mixed
     */
    public function notifyModerator(Job $job);

    /**
     * @param Job $job
     * @return mixed
     */
    public function notifyAuthor(Job $job);
}
