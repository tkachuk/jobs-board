<?php

namespace App\Services;

use App\Models\Job;

interface JobManagerInterface
{
    /**
     * @param array $data
     *
     * @return Job
     */
    public function create(array $data): Job;

    /**
     * @param int $id
     * @param string $hash
     * @param string $status
     *
     * @return Job|null
     */
    public function updateStatus(int $id, string $hash, string $status): ?Job;
}
