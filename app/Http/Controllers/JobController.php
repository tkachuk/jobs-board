<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Repositories\JobRepository;
use App\Services\JobManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class JobController extends Controller
{
    /**
     * @var JobRepository
     */
    private $jobRepository;

    /**
     * @var JobManager
     */
    private $jobManager;
    /**
     * @param JobRepository $jobRepository
     */
    public function __construct(JobRepository $jobRepository, JobManager $jobManager)
    {
        $this->jobRepository = $jobRepository;
        $this->jobManager = $jobManager;
    }

    /**
     * Display a jobs listing(approved only)
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->jobRepository->allApproved();
    }

    /**
     * Store a new posted job
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO add request validation

        return $this->jobManager->create($request->toArray());
    }

    /**
     * @param int $id
     * @param string $hash
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function approve(int $id, string $hash)
    {
       $job = $this->jobManager->updateStatus($id, $hash, Job::STATUS_APPROVED);

        if (!$job) {
            return response('Job not found', Response::HTTP_NOT_FOUND);
        }

        return response('Job has been approved.');
    }

    /**
     * @param int $id
     * @param string $hash
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function spam(int $id, string $hash)
    {
        $job =  $this->jobManager->updateStatus($id, $hash, Job::STATUS_SPAM);

        if (!$job) {
            return response('Job not found', Response::HTTP_NOT_FOUND);
        }

        return response('Job has been marked as spam');
    }
}
