<?php

namespace App\Observers;

use App\Models\Job;
use App\Services\JobPostingEmailNotifier;
use App\Services\JobPostingNotifierInterface;
use Illuminate\Support\Str;

class JobObserver
{

    /**
     * @var JobPostingNotifierInterface
     */
    private $jobPostingNotifier;

    public function __construct(JobPostingEmailNotifier $jobPostingNotifier)
    {
        $this->jobPostingNotifier = $jobPostingNotifier;
    }

    /**
     * Listen to the Job created event.
     *
     * @param  Job  $job
     * @return void
     */
    public function created(Job $job): void
    {
        if ($job->status === Job::STATUS_NEW) {
            $this->jobPostingNotifier->notifyModerator($job);
            $this->jobPostingNotifier->notifyAuthor($job);
        }
    }

    /**
     * @param Job $job
     */
    public function creating(Job $job): void
    {
        $job->hash = Str::random(32);
    }
}
