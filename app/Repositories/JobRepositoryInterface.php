<?php

namespace App\Repositories;

use App\Models\Job;
use Illuminate\Support\Collection;

interface JobRepositoryInterface
{
    /**
     * @return Collection
     */
    public function allApproved(): Collection;

    /**
     * @param string $email
     *
     * @return Collection
     */
    public function allApprovedByEmail(string $email): Collection;

    /**
     * @param int $id
     * @param string $hash
     *
     * @return Job|null
     */
    public function findByIdAndHash(int $id, string $hash): ?Job;
}
