<?php

namespace App\Repositories;

use App\Models\Job;
use Illuminate\Support\Collection;

class JobRepository extends BaseRepository implements JobRepositoryInterface
{

    /**
     * @param Job $model
     */
    public function __construct(Job $model)
    {
        parent::__construct($model);
    }

    /**
     * {@inheritDoc}
     */
    public function allApproved(): Collection
    {
        return $this->model->where('status', Job::STATUS_APPROVED)->get();
    }

    /**
     * {@inheritDoc}
     */
    public function findByIdAndHash(int $id, string $hash): ?Job
    {
        return $this->model
            ->where('id', $id)
            ->where('hash', $hash)
            ->first();
    }

    /**
     * {@inheritDoc}
     */
    public function allApprovedByEmail(string $email): Collection
    {
        return $this->model
            ->where('status', Job::STATUS_APPROVED)
            ->where('email', $email)
            ->get();
    }
}
