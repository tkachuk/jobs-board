<?php

namespace App\Providers;

use App\Models\Job;
use App\Observers\JobObserver;
use App\Services\ConfigRetriever;
use App\Services\JobPostingEmailNotifier;
use App\Services\JobPostingNotifierInterface;
use Illuminate\Config\Repository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->getContainer()->singleton(ConfigRetriever::class, function () {
            return new ConfigRetriever($this->getConfig());
        });

        $this->getContainer()->bind(JobPostingNotifierInterface::class, JobPostingEmailNotifier::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Job::observe(JobObserver::class);
    }

    /**
     * @return Repository
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    protected function getConfig(): Repository
    {
        return $this->getContainer()->get('config');
    }

    protected function getContainer()
    {
        return $this->app;
    }

}
