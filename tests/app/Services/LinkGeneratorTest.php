<?php

use App\Services\ConfigRetriever;
use App\Models\Job;
use App\Services\LinkGenerator;
use Tests\TestCase;

class LinkGeneratorTest extends TestCase
{
    /**
     * @var LinkGenerator
     */
    private $linkGenerator;

    public function setUp(): void
    {
        parent::setUp();

        $this->configretriever = $this->getMockBuilder(ConfigRetriever::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->linkGenerator = new LinkGenerator($this->configretriever);
    }

    /**
     * @dataProvider provider
     */
    public function testGenerateApproveLink($id, $hash, $host, $approveLink, $spamLink): void
    {
        $job = new Job();
        $job->id = $id;
        $job->hash = $hash;

        $this->configretriever
            ->method('getApplicationUrl')
            ->will($this->returnValue($host));

        $this->assertEquals($this->linkGenerator->getApproveLink($job), $approveLink);
        $this->assertEquals($this->linkGenerator->getSpamLink($job), $spamLink);
    }

    public function provider()
    {
        return [
            [
                1,
                'foo',
                 'http://localhost',
                 'http://localhost/api/jobs/approve/1/foo',
                 'http://localhost/api/jobs/spam/1/foo',
            ],
            [
                2,
                'bar',
                'http://localhost',
                'http://localhost/api/jobs/approve/2/bar',
                'http://localhost/api/jobs/spam/2/bar',
            ],
        ];
    }
}
