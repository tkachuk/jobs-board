## Overview

Simple Laravel+Vue application

## Task
* The project is to implement a web service with composer based PHP framework and Angular or Vuejs for the frontend.
* The service provides the UI interface to posting jobs(HR manager). 
* The service provides the possibility to approve/reject posts(moderator).

## Installation

### Docker & Docker Compose

#### Ubuntu

Look here https://docs.docker.com/engine/install/ubuntu/

#### MacOS

Look here https://docs.docker.com/docker-for-mac/

### Install dependencies

```sh
$ cd <project folder>
$ make install
```

or
```sh
$ cd <project folder>
$ docker-compose up -d
```

### Start project

```sh
$ cd <project folder>
$ make up
```

## Access container shell

```sh
$ docker exec -it app /bin/bash
```

## Testing

```sh
$ docker-compose exec app vendor/bin/phpunit
```

### Usage
* http://localhost
