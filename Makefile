.PHONY: build ## Build or rebuild application image
build: .env
	docker-compose build

.PHONY: install ## Install backend and frontend dependencies
install: install-backend install-frontend
	docker run --tty --rm -e 'COMPOSER_AUTH=${COMPOSER_AUTH}' -v ${PWD}:/app composer:1 composer install --ignore-platform-reqs

.PHONY: install-backend ## Install composer dependencies
install-backend:
	docker run --tty --rm -e 'COMPOSER_AUTH=${COMPOSER_AUTH}' -v ${PWD}:/app composer:1 composer install --ignore-platform-reqs

.PHONY: install-frontend ## Install frontend dependencies
install-frontend:
	docker run --tty --rm  -v ${PWD}:/var/www  wiwatsrt/docker-laravel-nodejs npm install

.PHONY: up ## Start all containers
up: .env
	docker-compose up -d
	docker-compose exec app php artisan key:generate
	docker-compose exec app php artisan migrate

.PHONY: down
down: ## Down all services
	docker-compose down

.PHONY: .env
.env: ## Create .env file
	cp .env.dist .env
