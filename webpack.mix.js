// webpack.mix.js

let mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js').vue();
mix.css('resources/css/app.css', 'public/css');
